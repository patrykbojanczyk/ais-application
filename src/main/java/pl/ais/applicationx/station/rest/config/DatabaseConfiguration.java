package pl.ais.applicationx.station.rest.config;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;

import java.util.Properties;

@Configuration
@PropertySource("classpath:application.properties")
public class DatabaseConfiguration {
    @Value( "${db.url}" ) private String url;
    @Value( "${db.driver}" ) private String driver;
    @Value( "${db.user}" ) private String user;
    @Value( "${db.password}" ) private String password;
    @Value( "${hibernate.service.show_sql}" ) private String isShowSQL;
    @Value( "${hibernate.service.dialect}" ) private String dialect;
    @Value( "${hibernate.service.hbm2ddl.auto}" ) private String hbm2ddlAuto;
    @Value( "${hibernate.enable_lazy_load_no_trans}" ) private String loadLazy;

    @Bean
    public BasicDataSource dataSource() {

        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(driver);
        dataSource.setUrl(url);
        dataSource.setUsername(user);
        dataSource.setPassword(password);

        return dataSource;
    }

    @Bean
    public LocalSessionFactoryBean sessionFactory(BasicDataSource basicDataSource) {

        LocalSessionFactoryBean localSessionFactoryBean = new LocalSessionFactoryBean();
        localSessionFactoryBean.setPackagesToScan("pl.ais.applicationx.station.rest.repository.model");

        Properties properties = new Properties();
        properties.setProperty("hibernate.show_sql", isShowSQL);
        properties.setProperty("hibernate.dialect", dialect);
        properties.setProperty("hibernate.hbm2ddl.auto", hbm2ddlAuto);
        properties.setProperty("hibernate.enable_lazy_load_no_trans", loadLazy);

        localSessionFactoryBean.setHibernateProperties(properties);

        localSessionFactoryBean.setDataSource(basicDataSource);

        return localSessionFactoryBean;
    }
}
