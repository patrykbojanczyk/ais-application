package pl.ais.applicationx.station.rest.repository.dao;

import pl.ais.applicationx.station.rest.repository.model.SheduleItemToStation;

import java.util.List;

public interface IProgramsDao {
    public List<SheduleItemToStation> getProgramsInTimePoint(Long timePoint);
    public SheduleItemToStation getNextProgramInShedule(Long id);
}
