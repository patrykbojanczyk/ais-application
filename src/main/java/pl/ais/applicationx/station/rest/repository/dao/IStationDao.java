package pl.ais.applicationx.station.rest.repository.dao;

import pl.ais.applicationx.station.rest.repository.model.SheduleItemToStation;
import pl.ais.applicationx.station.rest.repository.model.Station;

import java.util.List;

public interface IStationDao {
    public List<Station> getAll();
    public Station getStation(Long id);
    public List<SheduleItemToStation> getStationInfoWithShedule(Long id, Long startTime, Long endTime);
}
