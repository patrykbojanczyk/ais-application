package pl.ais.applicationx.station.rest.repository.dao;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.ais.applicationx.station.rest.repository.model.SheduleItemToStation;

import java.util.List;
import java.util.Objects;

@Repository
public class ProgramsDao extends HibernateDaoSupport implements IProgramsDao {

    @Autowired
    public ProgramsDao(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    @Override
    @Transactional
    public List<SheduleItemToStation> getProgramsInTimePoint(Long timePoint) {
        return getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(
                "FROM SheduleItemToStation s WHERE s.startTime<=:time AND s.id in (" +
                        "SELECT min(ss.id)" +
                        " FROM SheduleItemToStation ss" +
                        " WHERE ss.startTime = (" +
                        "       SELECT max(sss.startTime)" +
                        "       FROM SheduleItemToStation sss" +
                        "       WHERE sss.station.id = ss.station.id" +
                        "           AND sss.startTime<=:timee)" +
                        " GROUP BY ss.station.id)")
                .setLong("time", timePoint)
                .setLong("timee", timePoint)
                .list();
    }

    @Override
    @Transactional
    public SheduleItemToStation getNextProgramInShedule(Long id) {
        SheduleItemToStation current = getSheduleItemToStation(id);
        if(Objects.isNull(current)) {
            return null;
        }
        List<SheduleItemToStation> list = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(
                        "FROM SheduleItemToStation s " +
                            "WHERE s.station.id=:stationId " +
                            "AND s.startTime>:startTime " +
                            "ORDER BY s.startTime")
                        .setLong("stationId", current.getStation().getId())
                        .setLong("startTime", current.getStartTime())
                        .setMaxResults(1)
                        .list();
        if(Objects.isNull(list) || list.size()>1) {
            return null;
        }
        return list.get(0);
    }

    private SheduleItemToStation getSheduleItemToStation(Long id) {
        return getHibernateTemplate().load(SheduleItemToStation.class, id);
    }
}
