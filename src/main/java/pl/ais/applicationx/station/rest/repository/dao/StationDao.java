package pl.ais.applicationx.station.rest.repository.dao;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import pl.ais.applicationx.station.rest.repository.model.SheduleItemToStation;
import pl.ais.applicationx.station.rest.repository.model.Station;

import java.util.List;

@Repository
public class StationDao extends HibernateDaoSupport implements IStationDao {

    @Autowired
    public StationDao(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    @Override
    public List<Station> getAll() {
        DetachedCriteria criteria = DetachedCriteria.forClass(Station.class);
        criteria.addOrder(Order.asc("name"));

        return (List<Station>) getHibernateTemplate().findByCriteria(criteria);
    }

    @Override
    public Station getStation(Long id) {
        return getHibernateTemplate().load(Station.class, id);
    }

    @Override
    public List<SheduleItemToStation> getStationInfoWithShedule(Long id, Long startTime, Long endTime) {
        return (List<SheduleItemToStation>) getHibernateTemplate().find(
                "FROM SheduleItemToStation WHERE station.id = ? AND startTime>=? AND startTime<?",
                new Object[]{ id, startTime, endTime }
        );
    }

}
