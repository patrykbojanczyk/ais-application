package pl.ais.applicationx.station.rest.repository.model;


import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "tab_sheduleItemToStation")
@AssociationOverrides({
        @AssociationOverride(name = "station", joinColumns = @JoinColumn(name = "STATION_ID")),
        @AssociationOverride(name = "sheduleItem", joinColumns = @JoinColumn(name = "SHEDULE_ITEM_ID")),
})
public class SheduleItemToStation implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private Station station;

    @ManyToOne
    private SheduleItem sheduleItem;

    @Column(name = "START_TIME")
    private Long startTime;

    public SheduleItemToStation() {
    }

    public SheduleItemToStation(
            Long id,
            Station station,
            SheduleItem sheduleItem,
            Long startTime
    ) {
        this.id = id;
        this.station = station;
        this.sheduleItem = sheduleItem;
        this.startTime = startTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public SheduleItem getSheduleItem() {
        return sheduleItem;
    }

    public void setSheduleItem(SheduleItem sheduleItem) {
        this.sheduleItem = sheduleItem;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }
}
