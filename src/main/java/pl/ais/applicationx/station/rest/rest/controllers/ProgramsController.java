package pl.ais.applicationx.station.rest.rest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.ais.applicationx.station.rest.rest.exceptions.BadArgumentRestProgramException;
import pl.ais.applicationx.station.rest.rest.exceptions.RestProgramException;
import pl.ais.applicationx.station.rest.rest.model.RestProgram;
import pl.ais.applicationx.station.rest.rest.services.IProgramService;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/rest/programs")
public class ProgramsController {

    @Autowired private IProgramService programService;

    @RequestMapping(
            method = RequestMethod.GET,
            produces = "application/json"
    )
    public ResponseEntity<List<RestProgram>> getProgramsInTimePoint(
            @RequestParam(value = "date") String timepoint
    ) {
        try {
            return Optional.ofNullable(programService.getProgramsInTimePoint(timepoint))
                    .map(list -> new ResponseEntity<>(list, HttpStatus.OK))
                    .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
        } catch (ParseException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (BadArgumentRestProgramException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (RestProgramException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
