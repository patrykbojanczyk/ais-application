package pl.ais.applicationx.station.rest.rest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.ais.applicationx.station.rest.rest.exceptions.BadArgumentRestStationException;
import pl.ais.applicationx.station.rest.rest.exceptions.NotFoundRestStationException;
import pl.ais.applicationx.station.rest.rest.exceptions.RestStationException;
import pl.ais.applicationx.station.rest.rest.model.RestStation;
import pl.ais.applicationx.station.rest.rest.model.RestStationShedule;
import pl.ais.applicationx.station.rest.rest.services.IStationService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/rest/station")
public class StationController {

    @Autowired
    IStationService stationService;

    @RequestMapping(
            value = "/all",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    public ResponseEntity<List<RestStation>> getAll() {
        return Optional.ofNullable(stationService.getAll())
                .map(list -> new ResponseEntity<>(list, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @RequestMapping(
            value = "/info",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    public ResponseEntity<RestStationShedule> getStationInfo(
            @RequestParam(value = "id") Long stationId
    ) {
        try {
            return Optional.ofNullable(stationService.getStationWith24hShedule(stationId))
                    .map(info -> new ResponseEntity<>(info, HttpStatus.OK))
                    .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
        } catch (BadArgumentRestStationException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (NotFoundRestStationException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (RestStationException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    
}
