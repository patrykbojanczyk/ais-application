package pl.ais.applicationx.station.rest.rest.exceptions;

public class BadArgumentRestProgramException extends RestProgramException {
    public BadArgumentRestProgramException(String message) {
        super(message);
    }
}
