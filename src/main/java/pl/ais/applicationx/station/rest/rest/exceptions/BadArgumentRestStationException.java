package pl.ais.applicationx.station.rest.rest.exceptions;

public class BadArgumentRestStationException extends RestStationException {
    public BadArgumentRestStationException(String msg) {
        super(msg);
    }
}
