package pl.ais.applicationx.station.rest.rest.exceptions;

public class NotFoundRestStationException extends RestStationException {
    public NotFoundRestStationException(String msg) {
        super(msg);
    }
}
