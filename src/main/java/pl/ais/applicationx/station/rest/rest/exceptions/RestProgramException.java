package pl.ais.applicationx.station.rest.rest.exceptions;

public abstract class RestProgramException extends Exception {
    public RestProgramException(String message) {
        super(message);
    }
}
