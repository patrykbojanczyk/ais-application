package pl.ais.applicationx.station.rest.rest.exceptions;

public abstract class RestStationException extends Exception {
    public RestStationException(String msg) {
        super(msg);
    }
}
