package pl.ais.applicationx.station.rest.rest.model;


public class RestProgram {
    private String name;
    private String description;
    private Long duration;
    private Long timeToEnd;

    public RestProgram(
            String name,
            String description,
            Long duration,
            Long timeToEnd
    ) {
        this.name = name;
        this.description = description;
        this.duration = duration;
        this.timeToEnd = timeToEnd;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Long getTimeToEnd() {
        return timeToEnd;
    }

    public void setTimeToEnd(Long timeToEnd) {
        this.timeToEnd = timeToEnd;
    }
}
