package pl.ais.applicationx.station.rest.rest.model;

import pl.ais.applicationx.station.rest.repository.model.SheduleItemToStation;

import java.sql.Timestamp;

public class RestSheduleItem {
    private String name;
    private String description;
    private Timestamp startTime;

    public RestSheduleItem(
            String name,
            String description,
            Timestamp startTime
    ) {
        this.name = name;
        this.description = description;
        this.startTime = startTime;
    }

    public RestSheduleItem(
            SheduleItemToStation sheduleItemToStation
    ) {
        this.name = sheduleItemToStation.getSheduleItem().getName();
        this.description = sheduleItemToStation.getSheduleItem().getDescription();
        this.startTime = new Timestamp(sheduleItemToStation.getStartTime());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }
}
