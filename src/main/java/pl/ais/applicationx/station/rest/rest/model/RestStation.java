package pl.ais.applicationx.station.rest.rest.model;

import pl.ais.applicationx.station.rest.repository.model.Station;

public class RestStation {
    private Long id;
    private String name;

    public RestStation(
            Long id,
            String name
    ) {
        this.id = id;
        this.name = name;
    }

    public RestStation(
            Station station
    ) {
        this.id = station.getId();
        this.name = station.getName();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
