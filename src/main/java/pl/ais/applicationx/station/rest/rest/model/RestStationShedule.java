package pl.ais.applicationx.station.rest.rest.model;

import java.util.List;

public class RestStationShedule {
    private RestStation station;
    private List<RestSheduleItem> shedule;

    public RestStationShedule(
            RestStation station,
            List<RestSheduleItem> shedule
    ) {
        this.station = station;
        this.shedule = shedule;
    }

    public RestStation getStation() {
        return station;
    }

    public void setStation(RestStation station) {
        this.station = station;
    }

    public List<RestSheduleItem> getShedule() {
        return shedule;
    }

    public void setShedule(List<RestSheduleItem> shedule) {
        this.shedule = shedule;
    }
}
