package pl.ais.applicationx.station.rest.rest.services;

import pl.ais.applicationx.station.rest.rest.exceptions.RestProgramException;
import pl.ais.applicationx.station.rest.rest.model.RestProgram;

import java.text.ParseException;
import java.util.List;

public interface IProgramService {
    public List<RestProgram> getProgramsInTimePoint(String timePoint) throws RestProgramException, ParseException;
}
