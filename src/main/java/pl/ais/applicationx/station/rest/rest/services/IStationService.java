package pl.ais.applicationx.station.rest.rest.services;

import pl.ais.applicationx.station.rest.rest.exceptions.RestStationException;
import pl.ais.applicationx.station.rest.rest.model.RestStation;
import pl.ais.applicationx.station.rest.rest.model.RestStationShedule;

import java.util.List;

public interface IStationService {
    public List<RestStation> getAll();
    public RestStationShedule getStationWith24hShedule(Long stationId) throws RestStationException;
}
