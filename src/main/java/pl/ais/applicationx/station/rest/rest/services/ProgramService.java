package pl.ais.applicationx.station.rest.rest.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import pl.ais.applicationx.station.rest.repository.dao.IProgramsDao;
import pl.ais.applicationx.station.rest.repository.model.SheduleItemToStation;
import pl.ais.applicationx.station.rest.rest.exceptions.BadArgumentRestProgramException;
import pl.ais.applicationx.station.rest.rest.exceptions.RestProgramException;
import pl.ais.applicationx.station.rest.rest.model.RestProgram;
import pl.ais.applicationx.station.rest.rest.utils.converters.IDateConverter;
import pl.ais.applicationx.station.rest.rest.utils.converters.IProgramConverter;

import java.text.ParseException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProgramService implements IProgramService {

    private IProgramsDao programsDao;
    private IDateConverter dateConverter;
    private IProgramConverter programConverter;

    @Autowired
    public ProgramService(
            IProgramsDao programsDao,
            IDateConverter dateConverter,
            IProgramConverter programConverter
    ) {
        this.programsDao = programsDao;
        this.dateConverter = dateConverter;
        this.programConverter = programConverter;
    }


    @Override
    public List<RestProgram> getProgramsInTimePoint(String timePoint) throws RestProgramException, ParseException {
        if(Objects.isNull(timePoint) || !dateConverter.isValidFormat(timePoint)) {
            throw new BadArgumentRestProgramException("Wrong Arguments");
        }

        Date date = dateConverter.fromISO(timePoint);

        List<SheduleItemToStation> list = programsDao.getProgramsInTimePoint(date.getTime());

        return CollectionUtils.isEmpty(list) ? null : list.stream()
                                                        .map(item -> createRestProgram(item, date.getTime()))
                                                        .collect(Collectors.toList());
    }

    private RestProgram createRestProgram(SheduleItemToStation item, Long timePoint) {
        if(Objects.isNull(item)) {
            return null;
        }

        SheduleItemToStation nextItem = programsDao.getNextProgramInShedule(item.getId());
        if(Objects.isNull(nextItem) || nextItem.getStartTime()<item.getStartTime()) {
            return programConverter.convert(item, timePoint, null);
        }

        return programConverter.convert(item, timePoint, nextItem.getStartTime());
    }

}
