package pl.ais.applicationx.station.rest.rest.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.ais.applicationx.station.rest.repository.dao.IStationDao;
import pl.ais.applicationx.station.rest.repository.model.SheduleItemToStation;
import pl.ais.applicationx.station.rest.repository.model.Station;
import pl.ais.applicationx.station.rest.rest.exceptions.BadArgumentRestStationException;
import pl.ais.applicationx.station.rest.rest.exceptions.NotFoundRestStationException;
import pl.ais.applicationx.station.rest.rest.exceptions.RestStationException;
import pl.ais.applicationx.station.rest.rest.model.RestSheduleItem;
import pl.ais.applicationx.station.rest.rest.model.RestStation;
import pl.ais.applicationx.station.rest.rest.model.RestStationShedule;
import pl.ais.applicationx.station.rest.rest.utils.ITimeUtils;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class StationService implements IStationService {

    private IStationDao stationDao;
    private ITimeUtils timeUtils;

    @Autowired
    public StationService(
            IStationDao stationDao,
            ITimeUtils timeUtils
    ) {
        this.stationDao = stationDao;
        this.timeUtils = timeUtils;
    }

    @Override
    public List<RestStation> getAll() {
        List<Station> list = stationDao.getAll();

        if(Objects.isNull(list) || list.size()==0) {
            return null;
        } else {
            return list.stream()
                    .map(RestStation::new)
                    .collect(Collectors.toList());
        }

    }

    @Override
    public RestStationShedule getStationWith24hShedule(Long stationId) throws RestStationException {
        if(stationId == null || stationId <=0) {
            throw new BadArgumentRestStationException("Wrong Arguments");
        }
        Long startTime = timeUtils.getCurrentTime();
        Long endTime = timeUtils.getNext24hTime(startTime);
        List<SheduleItemToStation> sheduleItemToStations = stationDao.getStationInfoWithShedule(stationId, startTime, endTime);

        if(Objects.isNull(sheduleItemToStations) || sheduleItemToStations.size() == 0) {
            Station station = stationDao.getStation(stationId);

            if(Objects.isNull(station)) {
                throw new NotFoundRestStationException("Cannot find station.");
            }

            return new RestStationShedule(new RestStation(station), Collections.EMPTY_LIST);
        }

        return new RestStationShedule(
                new RestStation(sheduleItemToStations.get(0).getStation()),
                sheduleItemToStations.stream()
                    .filter(item -> startTime <= item.getStartTime() && endTime > item.getStartTime() && item.getStation().getId().equals(stationId))
                    .map(RestSheduleItem::new)
                    .collect(Collectors.toList())
        );
    }

}
