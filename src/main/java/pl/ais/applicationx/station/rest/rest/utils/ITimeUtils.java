package pl.ais.applicationx.station.rest.rest.utils;

public interface ITimeUtils {
    public Long getCurrentTime();
    public Long getNext24hTime(Long startTime);
}
