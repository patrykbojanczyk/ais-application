package pl.ais.applicationx.station.rest.rest.utils;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

@Component
public class TimeUtils implements ITimeUtils {

    @Override
    public Long getCurrentTime() {
        return DateUtils.truncate(new Date(System.currentTimeMillis()), Calendar.MINUTE).getTime();
    }

    @Override
    public Long getNext24hTime(Long startTime) {
        if(Objects.isNull(startTime) || startTime < 0) {
            startTime = getCurrentTime();
        }
        return startTime + (24*3600000);
    }
}
