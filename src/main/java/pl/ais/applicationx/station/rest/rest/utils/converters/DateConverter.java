package pl.ais.applicationx.station.rest.rest.utils.converters;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import pl.ais.applicationx.station.rest.rest.utils.ITimeUtils;

import java.text.ParseException;
import java.util.Date;
import java.util.Objects;

@Component
public class DateConverter implements IDateConverter {

    private final String regex;
    private final String[] patterns;
    private final String gmtMark;
    private ITimeUtils timeUtils;

    @Autowired
    public DateConverter(
            ITimeUtils timeUtils
    ) {
        this.timeUtils = timeUtils;
        this.regex = "^\\d{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])-([0-1][0-9]|2[0-3])-([0-5][0-9])([-,+](0[0-9]:(00|30)|1[0-1]:(00|30)|12:00))*$";
        this.patterns = new String[]{ "yyyy-MM-dd-HH-mmZZ", "yyyy-MM-dd-HH-mm" };
        this.gmtMark = "GMT";
    }

    @Override
    public boolean isValidFormat(String date) {
        if(StringUtils.isEmpty(date) || !date.matches(regex)) {
            return false;
        }
        return true;
    }

    @Override
    public Date fromISO(String date) {
        if(isValidFormat(date)) {
            try {
                if(date.length() == patterns[1].length()) {
                    date = unlocaleDate(date);
                }
                return DateUtils.parseDateStrictly(date, patterns);
            } catch (ParseException e) {
                return toDate(timeUtils.getCurrentTime());
            }
        } else {
            return toDate(timeUtils.getCurrentTime());
        }
    }

    @Override
    public Long getTime(Date date) {
        if(Objects.isNull(date)) {
            return timeUtils.getCurrentTime();
        }
        return date.getTime();
    }

    private Date toDate(Long time) {
        return new Date(time);
    }

    private String unlocaleDate(String date) {
        return date += gmtMark;
    }
}
