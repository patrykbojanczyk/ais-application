package pl.ais.applicationx.station.rest.rest.utils.converters;

import java.text.ParseException;
import java.util.Date;

public interface IDateConverter {
    public boolean isValidFormat(String date);
    public Date fromISO(String date) throws ParseException;
    public Long getTime(Date date);
}
