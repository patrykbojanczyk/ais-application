package pl.ais.applicationx.station.rest.rest.utils.converters;

import pl.ais.applicationx.station.rest.repository.model.SheduleItemToStation;
import pl.ais.applicationx.station.rest.rest.model.RestProgram;

public interface IProgramConverter {
    public RestProgram convert(SheduleItemToStation item, Long timePoint, Long startNextItem);
    public Long calculateDuration(Long time1, Long time2);
    public boolean validateTimes(Long time1, Long time2);
}
