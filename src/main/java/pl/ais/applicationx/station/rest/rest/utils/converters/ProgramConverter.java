package pl.ais.applicationx.station.rest.rest.utils.converters;

import org.springframework.stereotype.Component;
import pl.ais.applicationx.station.rest.repository.model.SheduleItemToStation;
import pl.ais.applicationx.station.rest.rest.model.RestProgram;

import java.util.Objects;

@Component
public class ProgramConverter implements IProgramConverter {
    @Override
    public RestProgram convert(SheduleItemToStation item, Long timePoint, Long startNextItem) {
        if(Objects.isNull(item) || Objects.isNull(item.getSheduleItem())) {
            return null;
        }
        return new RestProgram(
                item.getSheduleItem().getName(),
                item.getSheduleItem().getDescription(),
                calculateDuration(timePoint, item.getStartTime()),
                calculateDuration(timePoint, startNextItem));
    }

    @Override
    public Long calculateDuration(Long time1, Long time2) {
        if(!validateTimes(time1, time2)) {
            return 0L;
        }
        return time1>=time2 ? time1-time2 : time2-time1;
    }

    @Override
    public boolean validateTimes(Long time1, Long time2) {
        return Objects.nonNull(time1) && Objects.nonNull(time2) && time1>=0 && time2>=0;
    }
}
