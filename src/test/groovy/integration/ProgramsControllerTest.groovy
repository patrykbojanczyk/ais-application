package integration

import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.HttpResponseException
import org.apache.http.HttpStatus
import org.hibernate.Session
import org.hibernate.SessionFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.IntegrationTest
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.context.annotation.PropertySource
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.web.WebAppConfiguration
import pl.ais.applicationx.station.rest.Application
import pl.ais.applicationx.station.rest.repository.model.SheduleItem
import pl.ais.applicationx.station.rest.repository.model.SheduleItemToStation
import pl.ais.applicationx.station.rest.repository.model.Station
import pl.ais.applicationx.station.rest.rest.utils.ITimeUtils
import spock.lang.Specification
import spock.lang.Unroll

@ContextConfiguration(loader = SpringApplicationContextLoader.class, classes = [Application.class])
@WebAppConfiguration
@IntegrationTest
@PropertySource("classpath:application.properties")
class ProgramsControllerTest extends Specification{

    @Value('${server.port}') private String port;
    @Value('${server.url}') private String url;

    private final Long time = 1460656800000L;
    private final String stringDate = "2016-04-14-20-00+02:00";
    private final Long tenMinutes = 6600000L;

    private static Session session;

    private HTTPBuilder http;

    @Autowired private SessionFactory sessionFactory;
    @Autowired private ITimeUtils timeUtils;

    def setup() {
        http = new HTTPBuilder(String.format("http://%s:%s", url, port));
        session = sessionFactory.openSession();
    }

    def cleanupSpec() {
        session.createQuery("DELETE FROM SheduleItemToStation").executeUpdate();
        session.createQuery("DELETE FROM Station").executeUpdate();
        session.createQuery("DELETE FROM SheduleItem").executeUpdate();
        session.flush();
        session.close();
    }

    def "Should return NOT_FOUND if no programs"() {
        given:
            def responseStatus = null;

        when:
            try {
                http.get(
                        path: "/rest/programs",
                        query: [date: stringDate],
                        requestContentType: ContentType.JSON
                ) { header, body -> [body, header.responseBase.statusLine]};
            } catch (HttpResponseException e) {
                responseStatus = e.response.statusLine
            }

        then:
            responseStatus.statusCode == HttpStatus.SC_NOT_FOUND;
    }

    @Unroll
    def "Should return BAD_REQUEST if param is #date"() {
        given:
            def responseStatus = null;

        when:
            try {
                http.get(
                        path: "/rest/programs",
                        query: [date: date],
                        requestContentType: ContentType.JSON
                ) { header, body -> [body, header.responseBase.statusLine]};
            } catch (HttpResponseException e) {
                responseStatus = e.response.statusLine
            }

        then:
            responseStatus.statusCode == expectedCode;

        where:
            date                                |   expectedCode
            null                                |   HttpStatus.SC_BAD_REQUEST
            ""                                  |   HttpStatus.SC_BAD_REQUEST
            "empty"                             |   HttpStatus.SC_BAD_REQUEST
            "2016/04/14/20/00/+02:00"           |   HttpStatus.SC_BAD_REQUEST

    }

    def "Should return OK status with list of programs for specific timePoint"() {
        given:
            List<Station> stations = new ArrayList();
            List<SheduleItem> itemShedule = new ArrayList();
            List<SheduleItem> nextItemShedule = new ArrayList();
            List<SheduleItem> prevItemShedule = new ArrayList();
            List<Long> startTime = new ArrayList();
            List<Long> prevItemStart = new ArrayList();
            List<Long> nextItemStart = new ArrayList();

            for(int i=0; i<3; i++) {
                startTime.add(createStartTime());
                prevItemStart.add(startTime.get(i)-tenMinutes);
                nextItemStart.add(startTime.get(i)+tenMinutes);

                stations.add(new Station(-1, String.format("station%d",i)));

                prevItemShedule.add(new SheduleItem(-1, String.format("shedulePrevItem%d",i), "description"));
                itemShedule.add(new SheduleItem(-1, String.format("sheduleItem%d",i), "description"));
                nextItemShedule.add(new SheduleItem(-1, String.format("sheduleNextItem%d",i), "description"));

                def prevProgram = new SheduleItemToStation(-1, stations.get(i), prevItemShedule.get(i), prevItemStart.get(i));
                def program = new SheduleItemToStation(-1, stations.get(i), itemShedule.get(i), startTime.get(i));
                def nextProgram = new SheduleItemToStation(-1, stations.get(i), nextItemShedule.get(i), nextItemStart.get(i));

                session.save(stations.get(i));
                session.save(prevItemShedule.get(i));
                session.save(itemShedule.get(i));
                session.save(nextItemShedule.get(i));
                session.save(prevProgram);
                session.save(program);
                session.save(nextProgram);
            }
            session.flush();

        when:
            def (responseBody, responseStatus) = http.get(
                    path: "/rest/programs",
                    query: [date: stringDate],
                    requestContentType: ContentType.JSON
            ) { header, body -> [body, header.responseBase.statusLine]};

        then:
            responseStatus.statusCode == HttpStatus.SC_OK;
            responseBody != null;
            responseBody.size() == 3;
            responseBody.get(0).name == itemShedule.get(0).name;
            responseBody.get(0).duration == time-startTime.get(0);
            responseBody.get(0).timeToEnd == nextItemStart.get(0)-time;
            responseBody.get(1).name == itemShedule.get(1).name;
            responseBody.get(1).duration == time-startTime.get(1);
            responseBody.get(1).timeToEnd == nextItemStart.get(1)-time;
            responseBody.get(2).name == itemShedule.get(2).name;
            responseBody.get(2).duration == time-startTime.get(2);
            responseBody.get(2).timeToEnd == nextItemStart.get(2)-time;

    }

    private Long createStartTime() {
        return time - (600000*(new Random().nextInt(10)));
    }
}
