package integration

import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.HttpResponseException
import org.apache.http.HttpStatus
import org.hibernate.Session
import org.hibernate.SessionFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.IntegrationTest
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.context.annotation.PropertySource
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.web.WebAppConfiguration
import pl.ais.applicationx.station.rest.Application
import pl.ais.applicationx.station.rest.repository.model.SheduleItem
import pl.ais.applicationx.station.rest.repository.model.SheduleItemToStation
import pl.ais.applicationx.station.rest.repository.model.Station
import pl.ais.applicationx.station.rest.rest.utils.ITimeUtils
import spock.lang.Specification

@ContextConfiguration(loader = SpringApplicationContextLoader.class, classes = [Application.class])
@WebAppConfiguration
@IntegrationTest
@PropertySource("classpath:application.properties")
class StationControllerTest extends Specification{

    @Value('${server.port}') private String port;
    @Value('${server.url}') private String url;

    private static Session session;

    private HTTPBuilder http;

    @Autowired private SessionFactory sessionFactory;
    @Autowired private ITimeUtils timeUtils;

    def setup() {
        http = new HTTPBuilder(String.format("http://%s:%s", url, port));
        session = sessionFactory.openSession();
    }

    def cleanupSpec() {
        session.createQuery("DELETE FROM SheduleItemToStation").executeUpdate();
        session.createQuery("DELETE FROM Station").executeUpdate();
        session.createQuery("DELETE FROM SheduleItem").executeUpdate();
        session.flush();
        session.close();
    }

    def "/all Should return NOT_FOUND if no stations"() {
        given:
            def responseStatus = null;

        when:
            try {
                http.get(
                        path: "/rest/station/all",
                        requestContentType: ContentType.JSON
                ) { header, body -> [body, header.responseBase.statusLine]};
            } catch (HttpResponseException e) {
                responseStatus = e.response.statusLine
            }

        then:
            responseStatus.statusCode == HttpStatus.SC_NOT_FOUND;
    }

    def "/all Should return list of all stations"() {
        given:
            session.save(new Station(-1, "station1"))
            session.save(new Station(-1, "station5"))
            session.save(new Station(-1, "station3"))
            session.save(new Station(-1, "station2"))
            session.save(new Station(-1, "station4"))
            session.flush();

        when:
            def (responseBody, responseStatus) = http.get(
                    path: "/rest/station/all",
                    requestContentType: ContentType.JSON
                ) { header, body -> [body as List, header.responseBase.statusLine]};

        then:
            responseStatus.statusCode == HttpStatus.SC_OK;
            responseBody.size() == 5;
    }

    def "/all Should return sorted list of all stations"() {
        when:
            def (responseBody, responseStatus) = http.get(
                    path: "/rest/station/all",
                    requestContentType: ContentType.JSON
            ) { header, body -> [body as List, header.responseBase.statusLine]};

        then:
            responseStatus.statusCode == HttpStatus.SC_OK;
            responseBody.size() == 5;
            responseBody.get(0).name == "station1";
            responseBody.get(1).name == "station2";
            responseBody.get(2).name == "station3";
            responseBody.get(3).name == "station4";
            responseBody.get(4).name == "station5";
    }

    def "/info Should return BAD_REQUEST if stationId not exist"() {
        given:
            def responseStatus = null;

        when:
            try {
                http.get(
                        path: "/rest/station/info",
                        requestContentType: ContentType.JSON
                ) { header, body -> [body, header.responseBase.statusLine]};
            } catch (HttpResponseException e) {
                responseStatus = e.response.statusLine
            }

        then:
            responseStatus.statusCode == HttpStatus.SC_BAD_REQUEST
    }

    def "/info Should return BAD_REQUEST if stationId is incorrect"() {
        given:
            def responseStatus = null;

        when:
            try {
                http.get(
                        path: "/rest/station/info",
                        query: [id: -1],
                        requestContentType: ContentType.JSON
                ) { header, body -> [body, header.responseBase.statusLine]};
            } catch (HttpResponseException e) {
                responseStatus = e.response.statusLine
            }

        then:
            responseStatus.statusCode == HttpStatus.SC_BAD_REQUEST
    }


    def "/info Should return station info with empty shedule for next 24h if no programs for this station"() {
        given:
            Station station = new Station(-1, "stationN");
            session.save(station);
            session.flush();

        when:
            def (responseBody, responseStatus) = http.get(
                    path: "/rest/station/info",
                    query: [id: station.id],
                    requestContentType: ContentType.JSON
            ) { header, body -> [body, header.responseBase.statusLine]};

        then:
            responseStatus.statusCode == HttpStatus.SC_OK;
            responseBody.station != null;
            responseBody.station.id == station.id;
            responseBody.station.name == station.name;
            responseBody.shedule != null;
            responseBody.shedule.size() == 0;
    }

    def "/info Should return station info with shedule start with request time and end in next 24h"() {
        given:
            Station station = new Station(-1, "stationWithShedule");
            session.save(station);

            List<SheduleItem> mockShedule = new ArrayList();
            for(int i=0; i<100; i++) {
                mockShedule.add(new SheduleItem(-1, String.format("sheduleItem%d", i), "description"));
                session.save(mockShedule.get(i));
                session.save(new SheduleItemToStation(-1, station, mockShedule.get(i), timeUtils.getCurrentTime()+((i-10)*1800000)));
                println (i-10);
            }
            session.flush();

        when:
            def (responseBody, responseStatus) = http.get(
                    path: "/rest/station/info",
                    query: [id: station.id],
                    requestContentType: ContentType.JSON
            ) { header, body -> [body, header.responseBase.statusLine]};

        then:
            responseStatus.statusCode == HttpStatus.SC_OK;
            responseBody.station != null;
            responseBody.station.id == station.id;
            responseBody.station.name == station.name;
            responseBody.shedule != null;
            responseBody.shedule.size() == 48;
    }

    def "/info Should return station info with shedule for next 24h only for this station"() {
        given:
            Station station1 = new Station(-1, "stationWithShedule");
            Station station2 = new Station(-1, "stationWithoutShedule");
            session.save(station1);
            session.save(station2);

            List<SheduleItem> mockShedule = new ArrayList();
            for(int i=0; i<10; i++) {
                mockShedule.add(new SheduleItem(-1, String.format("sheduleItem%d", i), "description"));
                session.save(mockShedule.get(i));
                session.save(new SheduleItemToStation(-1, station1, mockShedule.get(i), timeUtils.getCurrentTime()+(i*1800000)));
            }
            session.save(new SheduleItemToStation(-1, station2, mockShedule.get(1), timeUtils.getCurrentTime()+(11*1800000)));
            session.flush();

        when:
            def (responseBody, responseStatus) = http.get(
                    path: "/rest/station/info",
                    query: [id: station1.id],
                    requestContentType: ContentType.JSON
            ) { header, body -> [body, header.responseBase.statusLine]};

        then:
            responseStatus.statusCode == HttpStatus.SC_OK;
            responseBody.station != null;
            responseBody.station.id == station1.id;
            responseBody.station.name == station1.name;
            responseBody.shedule != null;
            responseBody.shedule.size() == 10;
    }

}
