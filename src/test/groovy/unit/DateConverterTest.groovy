package unit

import org.apache.commons.lang.time.DateUtils
import pl.ais.applicationx.station.rest.rest.utils.converters.DateConverter
import pl.ais.applicationx.station.rest.rest.utils.converters.IDateConverter
import pl.ais.applicationx.station.rest.rest.utils.ITimeUtils
import pl.ais.applicationx.station.rest.rest.utils.TimeUtils
import spock.lang.Specification
import spock.lang.Unroll

import java.text.SimpleDateFormat

class DateConverterTest extends Specification {

    private IDateConverter dateConverter;
    private ITimeUtils timeUtils;

    def setup() {
        timeUtils = new TimeUtils();
        dateConverter = new DateConverter(timeUtils)
    }

    @Unroll
    def "isValidFormat Should return #valid if date is #date"() {
        when:
            def validationResult = dateConverter.isValidFormat(date);

        then:
            validationResult == valid;

        where:
            date                                |   valid
            null                                |   false
            ""                                  |   false
            "incorrect format"                  |   false
            "2016-15-14-20-00"                  |   false
            "2016-04-50-20-00"                  |   false
            "2016-04-14-30-00"                  |   false
            "2016-04-14-20-70"                  |   false
            "2016-04-14-20-0000"                |   false
            "2016\\04\\14-20:00"                |   false
            "2016-04-14-20-00+15:30"            |   false
            "2016-04-14-20-00*12:00"            |   false
            "2016-04-14-20-00-00*12:00"         |   false
            "2016-04-14-20-00-00.001+12:00"     |   false
            "2016-04-14-20-00"                  |   true
            "2016-04-14-20-00+01:00"            |   true
            "2016-04-14-20-00-01:00"            |   true
    }

    @Unroll
    def "fromISO Should return currentDate if date is #date"() {
        when:
            def returnDate = dateConverter.fromISO(date);

        then:
            currentDateResult == returnDate;

        where:
            date                        |   currentDateResult
            null                        |   DateUtils.truncate(new Date(System.currentTimeMillis()), Calendar.MINUTE)
            ""                          |   DateUtils.truncate(new Date(System.currentTimeMillis()), Calendar.MINUTE)
            "incorrect format"          |   DateUtils.truncate(new Date(System.currentTimeMillis()), Calendar.MINUTE)
            "2016-15-14-20-00"          |   DateUtils.truncate(new Date(System.currentTimeMillis()), Calendar.MINUTE)
            "2016-04-50-20-00"          |   DateUtils.truncate(new Date(System.currentTimeMillis()), Calendar.MINUTE)
            "2016-04-14-30-00"          |   DateUtils.truncate(new Date(System.currentTimeMillis()), Calendar.MINUTE)
            "2016-04-14-20-70"          |   DateUtils.truncate(new Date(System.currentTimeMillis()), Calendar.MINUTE)
            "2016-04-14-20-0000"        |   DateUtils.truncate(new Date(System.currentTimeMillis()), Calendar.MINUTE)
            "2016\\04\\14-20:00"        |   DateUtils.truncate(new Date(System.currentTimeMillis()), Calendar.MINUTE)
            "2016-04-14-20-00+15:30"    |   DateUtils.truncate(new Date(System.currentTimeMillis()), Calendar.MINUTE)
            "2016-04-14-20-00*12:00"    |   DateUtils.truncate(new Date(System.currentTimeMillis()), Calendar.MINUTE)
    }

    @Unroll
    def "fromISO Should return date from ISO8061 format if date is #date"() {
        when:
            def returnDate = dateConverter.fromISO(date);

        then:
            dateResult == returnDate;

        where:
            date                        |   dateResult
            "2016-04-14-20-00"          |   new SimpleDateFormat("yyyy-MM-dd HH:mmZ").parse("2016-04-14 20:00GMT")
            "2016-04-14-20-00+01:00"    |   new SimpleDateFormat("yyyy-MM-dd HH:mmZ").parse("2016-04-14 20:00+0100")
            "2016-04-14-20-00-01:00"    |   new SimpleDateFormat("yyyy-MM-dd HH:mmZ").parse("2016-04-14 20:00-0100")
    }

}
