package unit

import pl.ais.applicationx.station.rest.repository.model.SheduleItem
import pl.ais.applicationx.station.rest.repository.model.SheduleItemToStation
import pl.ais.applicationx.station.rest.rest.utils.converters.IProgramConverter
import pl.ais.applicationx.station.rest.rest.utils.converters.ProgramConverter
import spock.lang.Specification
import spock.lang.Unroll

class ProgramConverterTest extends Specification {

    private IProgramConverter programConverter;

    def setup() {
        programConverter = new ProgramConverter();
    }

    @Unroll
    def "convert Should return #expectedValue if #name is null"() {
        when:
            def returnValue = programConverter.convert(item, 0, 0);

        then:
            returnValue == expectedValue;

        where:
            name                    |   item                        |   expectedValue
            "item"                  |   null                        |   null
            "item.getSheduleItem()" |   new SheduleItemToStation()  |   null
    }

    @Unroll
    def "convert Should return RestProgram with duration equal #expectedValue if #incorrect is #incorrectValue"() {
        when:
            def item = new SheduleItemToStation(-1L, null, new SheduleItem(-1 , "name", "description"), startTime);
            def returnValue = programConverter.convert(item, timePoint, 1);

        then:
            returnValue.getDuration() == expectedValue as Long;

        where:
            name        |   incorrectValue  |   startTime  |   timePoint   |   expectedValue
            "startTime" |   "null"          |   null       |   1           |   0
            "startTime" |   "-1"            |   -1         |   1           |   0
            "timePoint" |   "null"          |   1          |   null        |   0
            "timePoint" |   "-1"            |   1          |   -1          |   0
    }

    @Unroll
    def "convert Should return RestProgram with timeToEnd equal #expectedValue if #incorrect is #incorrectValue"() {
        when:
            def item = new SheduleItemToStation(-1L, null, new SheduleItem(-1 , "name", "description"), 1);
            def returnValue = programConverter.convert(item, timePoint, timeNext);

        then:
            returnValue.getTimeToEnd() == expectedValue as Long;

        where:
            name        |   incorrectValue  |   timeNext   |   timePoint   |   expectedValue
            "timeNext"  |   "null"          |   null       |   1           |   0
            "timeNext"  |   "-1"            |   -1         |   1           |   0
            "timePoint" |   "null"          |   1          |   null        |   0
            "timePoint" |   "-1"            |   1          |   -1          |   0
    }

    @Unroll
    def "validateTimes Should return #expectedValue if time1 is #time1 and time2 is #time2"() {
        when:
            def returnValue = programConverter.validateTimes(time1, time2);

        then:
            returnValue == expectedValue;

        where:
            time1       |   time2       |   expectedValue
            null        |   1           |   false
            1           |   null        |   false
            null        |   -1          |   false
            -1          |   null        |   false
            null        |   null        |   false
            -1          |   1           |   false
            1           |   -1          |   false
            -1          |   1           |   false
            -1          |   -1          |   false
            0           |   1           |   true
            1           |   0           |   true
            1           |   1           |   true
    }

    @Unroll
    def "calculateDuration Should return #expectedValue if time1 is #time1 and time2 is #time2"() {
        when:
            def returnValue = programConverter.calculateDuration(time1, time2);

        then:
            returnValue == expectedValue as Long;

        where:
            time1       |   time2       |   expectedValue
            null        |   1           |   0
            1           |   null        |   0
            null        |   -1          |   0
            -1          |   null        |   0
            null        |   null        |   0
            -1          |   1           |   0
            1           |   -1          |   0
            -1          |   1           |   0
            -1          |   -1          |   0
            0           |   1           |   1
            1           |   0           |   1
            1           |   1           |   0
    }
}
