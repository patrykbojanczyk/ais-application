package unit

import pl.ais.applicationx.station.rest.repository.dao.IProgramsDao
import pl.ais.applicationx.station.rest.repository.model.SheduleItem
import pl.ais.applicationx.station.rest.repository.model.SheduleItemToStation
import pl.ais.applicationx.station.rest.repository.model.Station
import pl.ais.applicationx.station.rest.rest.exceptions.BadArgumentRestProgramException
import pl.ais.applicationx.station.rest.rest.model.RestProgram
import pl.ais.applicationx.station.rest.rest.services.IProgramService
import pl.ais.applicationx.station.rest.rest.services.ProgramService
import pl.ais.applicationx.station.rest.rest.utils.converters.IDateConverter
import pl.ais.applicationx.station.rest.rest.utils.converters.IProgramConverter
import spock.lang.Specification
import spock.lang.Unroll


class ProgramServiceTest extends Specification{

    private final Long time = 1460656800000L;
    private final Long timeWithoutResults = 1483142400000L;
    private final String stringDate = "2016-04-14-20-00+02:00";
    private final String stringDateWithoutResults = "2016-12-31-00-00+00:00";

    private IProgramService programService;
    private IProgramsDao programsDao;
    private IDateConverter dateConverter;
    private IProgramConverter programConverter;

    def setup() {
        programsDao = Mock(IProgramsDao);
        dateConverter = Mock(IDateConverter);
        programConverter = Mock(IProgramConverter)

        List<Station> mockStations = new ArrayList();
        List<SheduleItem> mockShedule = new ArrayList();
        List<SheduleItemToStation> mockSheduleToStation = new ArrayList();
        for(int i=0; i<10; i++) {
            mockStations.add(new Station(i+1, String.format("station%d",i)));
            mockShedule.add(new SheduleItem(i+1, String.format("sheduleItem%d",i), "description"));
            Long startTime = createStartTime();
            Long nextItemStart = startTime+6600000;
            Long duration = time - startTime;
            Long timeToEnd = nextItemStart - startTime;
            def item = new SheduleItemToStation(i+1, mockStations.get(i), mockShedule.get(i), startTime);
            def nextItem = new SheduleItemToStation(-1L, mockStations.get(i), null, nextItemStart);
            def restProgram = new RestProgram(item.getSheduleItem().getName(), item.getSheduleItem().getDescription(), duration, timeToEnd);
            mockSheduleToStation.add(item);
            programsDao.getNextProgramInShedule(i+1) >> nextItem;
            programConverter.calculateDuration(startTime, time) >> duration;
            programConverter.calculateDuration(startTime, nextItemStart) >> timeToEnd;
            programConverter.validateTimes(startTime, time) >> true
            programConverter.validateTimes(startTime, nextItemStart) >> true
            programConverter.convert(item, time, nextItemStart) >> restProgram;
        }

        def date = new Date(time);
        programsDao.getProgramsInTimePoint(time) >> mockSheduleToStation;
        dateConverter.fromISO(stringDate) >> date;
        dateConverter.getTime(date) >> time;
        dateConverter.isValidFormat(stringDate) >> true;

        def dateWithoutResults = new Date(timeWithoutResults);
        programsDao.getProgramsInTimePoint(timeWithoutResults) >> new LinkedList<SheduleItemToStation>();
        dateConverter.fromISO(stringDateWithoutResults) >> dateWithoutResults;
        dateConverter.getTime(dateWithoutResults) >> timeWithoutResults;
        dateConverter.isValidFormat(stringDateWithoutResults) >> true;

        programService = new ProgramService(programsDao, dateConverter, programConverter);
    }

    @Unroll
    def "Should throw RestProgramException if date is #date"() {
        given:
            def throwException = null;

        when:
            try {
                programService.getProgramsInTimePoint(date);
            } catch (Exception e) {
                throwException = e;
            }

        then:
            exception.getClass() == throwException.getClass();

        where:
            date                            |   exception
            null                            |   new BadArgumentRestProgramException("Wrong argument");
            ""                              |   new BadArgumentRestProgramException("Wrong argument");
            "2016/04/14/20/00/+02:00"       |   new BadArgumentRestProgramException("Wrong argument");
    }

    def "Should return null doesn't exists programs at specific time point"() {
        when:
            def list = programService.getProgramsInTimePoint(stringDateWithoutResults);

        then:
            list == null;
    }

    def "Should return list of all programs at specific time point"() {
        when:
            def list = programService.getProgramsInTimePoint(stringDate);

        then:
            list != null;
            list.size() == 10;
            list.get(0) != null;
            list.get(1) != null;
            list.get(2) != null;
            list.get(3) != null;
            list.get(4) != null;
            list.get(5) != null;
            list.get(6) != null;
            list.get(7) != null;
            list.get(8) != null;
            list.get(9) != null;
    }

    private Long createStartTime() {
        return time - (600000*(new Random().nextInt(10)));
    }

}
