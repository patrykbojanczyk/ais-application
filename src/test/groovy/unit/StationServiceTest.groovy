package unit

import pl.ais.applicationx.station.rest.repository.dao.IStationDao
import pl.ais.applicationx.station.rest.repository.model.SheduleItem
import pl.ais.applicationx.station.rest.repository.model.SheduleItemToStation
import pl.ais.applicationx.station.rest.repository.model.Station
import pl.ais.applicationx.station.rest.rest.exceptions.BadArgumentRestStationException
import pl.ais.applicationx.station.rest.rest.model.RestStation
import pl.ais.applicationx.station.rest.rest.model.RestStationShedule
import pl.ais.applicationx.station.rest.rest.services.IStationService
import pl.ais.applicationx.station.rest.rest.services.StationService
import pl.ais.applicationx.station.rest.rest.utils.ITimeUtils
import spock.lang.Specification
import spock.lang.Unroll

class StationServiceTest extends Specification {

    private IStationDao stationDao;
    private ITimeUtils timeUtils;
    private IStationService stationService;

    private final Long time = 1460656800000L;
    private final Long time24hLater = 1460743200000L;

    def setup() {
        stationDao = Mock(IStationDao);
        timeUtils = Mock(ITimeUtils);
            timeUtils.getCurrentTime() >> time;
            timeUtils.getNext24hTime(time) >> time24hLater;

        List<Station> mockStations = new ArrayList();;
        List<SheduleItem> mockShedule = new ArrayList();
        List<SheduleItemToStation> mockStationInfoWithShedule = new ArrayList();

        for(int i=1; i<4; i++) {
            mockStations.add(new Station(i, String.format("station%d", i)));
        }
        for(int i=1; i<7; i++) {
            mockShedule.add(new SheduleItem(i, String.format("sheduleItem%d", i), "description"));
        }
        for(int i=0; i<5; i++) {
            mockStationInfoWithShedule.add(new SheduleItemToStation(i as long, mockStations[i==4 ? 1 : 0], mockShedule[i], timeUtils.getCurrentTime()+((i+1)*1000)));
        }

        stationDao.getAll() >> mockStations;
        stationDao.getStationInfoWithShedule(1, time, time24hLater) >> mockStationInfoWithShedule;
        stationDao.getStation(1) >> mockStations[0];

        stationService = new StationService(stationDao, timeUtils);
    }

    def "getAll Should return stations converted to rest model"() {
        when:
            def stations = stationService.getAll();

        then:
            stations.get(0).class == RestStation.class;
            stations.get(1).class == RestStation.class;
            stations.get(2).class == RestStation.class;
    }

    def "getAll Should return stations as list"() {
        when:
            def stations = stationService.getAll();

        then:
            stations.size() == 3;
            stations.get(0).id == 1L;
            stations.get(0).name == "station1";
            stations.get(1).id == 2L;
            stations.get(1).name == "station2";
            stations.get(2).id == 3L;
            stations.get(2).name == "station3";
    }

    @Unroll
    def "getStationWith24hShedule Should throw exception if ID is #param"() {
        given:
            def throwException = null;

        when:
            try {
                stationService.getStationWith24hShedule(param);
            } catch (Exception e) {
                throwException = e;
            }

        then:
            exception.getClass() == throwException.getClass();

        where:
            param       ||   exception
            0           ||   new BadArgumentRestStationException("Wrong argument");
            -1          ||   new BadArgumentRestStationException("Wrong argument");
            null        ||   new BadArgumentRestStationException("Wrong argument");
    }

    def "getStationWith24hShedule Should return map with rest station and shedule as list"() {
        given:
            def id = 1L;
        when:
            def stationInfo = stationService.getStationWith24hShedule(id);

        then:
            stationInfo.shedule.size() == 4;
            stationInfo.station.id == id;
    }

    def "getStationWith24hShedule Should return empty list if station hasn't any programs"() {
        given:
            def id = [5, 6];
            stationDao.getStation(id[0]) >> new Station(id[0], "name");
            stationDao.getStation(id[1]) >> new Station(id[1], "name");
            stationDao.getStationInfoWithShedule(id[0], time, time24hLater) >> null;
            stationDao.getStationInfoWithShedule(id[1], time, time24hLater) >> Mock(List);

        when:
            RestStationShedule stationInfo1 = stationService.getStationWith24hShedule(id[0]);
            RestStationShedule stationInfo2 = stationService.getStationWith24hShedule(id[1]);

        then:
            stationInfo1.getStation() != null;
            stationInfo1.getStation().id == id[0] as long;
            stationInfo1.getShedule() != null;
            stationInfo1.getShedule().size() == 0;

            stationInfo2.getStation() != null;
            stationInfo2.getStation().id == id[1] as long;
            stationInfo2.getShedule() != null;
            stationInfo2.getShedule().size() == 0;
    }
}
