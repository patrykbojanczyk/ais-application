package unit

import pl.ais.applicationx.station.rest.rest.utils.ITimeUtils
import pl.ais.applicationx.station.rest.rest.utils.TimeUtils
import spock.lang.Specification

class TimeUtilsTest extends Specification {

    private ITimeUtils timeUtils;

    def setup() {
        timeUtils = new TimeUtils();
    }

    def "getCurrentTime Should return current time accurate to minutes"() {
        given:
            def calendar = Calendar.getInstance();
                calendar.set(Calendar.MILLISECOND, 0);
                calendar.set(Calendar.SECOND, 0);
            def expectedTime = calendar.timeInMillis;
        when:
            def time = timeUtils.getCurrentTime();

        then:
            expectedTime == time;
    }

    def "getNext24hTime Should add 24h to current time if param is null or less than 0"() {
        given:
            def calendar = Calendar.getInstance();
                calendar.set(Calendar.MILLISECOND, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.add(Calendar.HOUR, 24);
            def returnTime = calendar.timeInMillis;

        when:
            def time = timeUtils.getNext24hTime(-1);
            def time2 = timeUtils.getNext24hTime(null);

        then:
            time == returnTime;
            time2 == returnTime;
    }

    def "getNext24hTime Should add 24h to time in param"() {
        given:
            def calendar = Calendar.getInstance();
                calendar.set(Calendar.MILLISECOND, 0);
                calendar.set(Calendar.SECOND, 0);
            def currentTime = calendar.timeInMillis;
                calendar.add(Calendar.HOUR, 24);
            def expectedTime = calendar.timeInMillis;

        when:
            def time = timeUtils.getNext24hTime(currentTime);

        then:
            expectedTime == time;
    }
}
